export interface Product {
  name: string;
  options: string;
  category: string;
  tax_group: string;
  price: string;
}
