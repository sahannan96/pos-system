export interface Customer {
  phone: string;
  name: string;
  order_count: string;
  order_value: string;
}
