import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Customer } from '../interface/customer.interface';
import { CustomerService } from './customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
})
export class CustomerComponent implements OnInit {
  dataSource = new MatTableDataSource<Customer>();
  displayedColumns: any[] = ['phone', 'name', 'order_count', 'order_value'];

  constructor(
    private router: Router,
    private customerService: CustomerService
  ) {
    this.customerService.getAllCustomers().subscribe((data) => {
      this.dataSource.data = data;
      console.log(data);
    });
  }

  ngOnInit(): void {}
}
