import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GET_ALL_CUSTOMERS } from '../common/url-strings';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  constructor(private http: HttpClient) {}

  getAllCustomers() {
    return this.http
      .get<{ message: any[] }>(GET_ALL_CUSTOMERS, {
        headers: { Authorization: 'token ceb488698aee2c5:c45a9e13a3f6865' },
      })
      .pipe(
        map((res) => {
          return res.message;
        })
      );
  }
}
