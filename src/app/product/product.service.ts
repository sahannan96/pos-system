import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ADD_PRODUCTS, GET_ALL_PRODUCTS, PRODUCT } from '../common/url-strings';
import { map } from 'rxjs/operators';
import { AUTHORIZATION, TOKEN } from '../common/constants';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private http: HttpClient) {}

  getAllProducts() {
    const headers = { [AUTHORIZATION]: TOKEN };
    return this.http
      .get<{ message: any[] }>(GET_ALL_PRODUCTS, {
        headers,
      })
      .pipe(
        map((res) => {
          return res.message;
        })
      );
  }

  addProduct(products) {
    const products1 = JSON.stringify(products.products[0]);

    const headers = { [AUTHORIZATION]: TOKEN };
    return this.http.post(ADD_PRODUCTS, products1, {
      headers,
    });
  }

  getProduct(id) {
    const headers = { [AUTHORIZATION]: TOKEN };
    return this.http.get<any>(PRODUCT + '/' + id, {
      headers,
    });
  }

  deleteProduct(id) {
    const headers = { [AUTHORIZATION]: TOKEN };
    return this.http.delete<any>(PRODUCT + '/' + id, {
      headers,
    });
  }

  updateProduct(product) {
    const headers = { [AUTHORIZATION]: TOKEN };
    return this.http.put<any>(PRODUCT + '/' + product.name, product, {
      headers,
    });
  }
}
