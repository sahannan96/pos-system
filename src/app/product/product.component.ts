import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Product } from '../interface/product.interface';
import { ProductService } from './product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  dataSource = new MatTableDataSource<Product>();
  displayedColumns: any[] = [
    'checkbox',
    'name',
    'options',
    'category',
    'tax_group',
    'price',
  ];

  selection = new SelectionModel<Product>(true, []);

  constructor(
    private productService: ProductService,
    public dialog: MatDialog
  ) {
    this.productService.getAllProducts().subscribe((data) => {
      this.dataSource.data = data;
      console.log(this.dataSource.data)
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  removeProducts() {
    console.log(this.selection.selected);
  }

  ngOnInit(): void {}
}
